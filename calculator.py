import sys
class Calculator():

    def addition(self, x, y):
        return x + y


    def subtract(self, x, y):
        return x - y


    def multiply(self, x, y):
        return x * y


    def divide(self, x, y):
        try:
            result = x / y
            return result
        except ZeroDivisionError as e:
            print(e)

calculator = Calculator()
if sys.argv[1] == "add":
    print(calculator.addition(int(sys.argv[2]), int(sys.argv[3])))
if sys.argv[1] == "sub":
    print(calculator.subtract(int(sys.argv[2]), int(sys.argv[3])))
if sys.argv[1] == "div":
    print(calculator.divide(int(sys.argv[2]), int(sys.argv[3])))
if sys.argv[1] == "mul":
    print(calculator.multiply(int(sys.argv[2]), int(sys.argv[3])))
